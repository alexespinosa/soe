package com.erp.soe.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

public class PrimeServiceTest {

    PrimeService primeService = new PrimeService();

    @Test
    public void writePrimes_HappyPath() throws IOException {
        byte[] data = ("3" + System.lineSeparator() + "stop" + System.lineSeparator()).getBytes();
        InputStream input = new ByteArrayInputStream(data);
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(arrayOutputStream));
        primeService.writePrimes(reader, writer);
        Assertions.assertTrue( ("2 "+System.lineSeparator()).equals(new String(arrayOutputStream.toByteArray())));
    }

    @Test
    public void writePrimes_InvalidInput() throws IOException {
        byte[] data = ("2" + System.lineSeparator() + "stop" + System.lineSeparator()).getBytes();
        InputStream input = new ByteArrayInputStream(data);
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(arrayOutputStream));
        primeService.writePrimes(reader, writer);
        Assertions.assertTrue( ("Please enter an integer greater than 2."+System.lineSeparator()).equals(new String(arrayOutputStream.toByteArray())));
    }

    @Test
    public void generatePrimes_HappyPath() throws IOException {
        Assertions.assertTrue( ("2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 ").equals(primeService.generatePrimes("50")));
    }

    @Test
    public void isValidInput_HappyPath(){
        Assertions.assertTrue(primeService.isValidInput("3"));
    }

    @Test
    public void isValidInput_NegativeValue(){
        Assertions.assertFalse(primeService.isValidInput("-3"));
    }
    @Test
    public void isValidInput_NonInteger(){
        Assertions.assertFalse(primeService.isValidInput("invalid"));
    }

    @Test
    public void isValidInput_NotGreaterThan2(){
        Assertions.assertFalse(primeService.isValidInput("1"));
    }
    @Test
    public void isValidInput_Null(){
        Assertions.assertFalse(primeService.isValidInput(null));
    }
}
