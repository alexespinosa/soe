package com.erp.employee;

import java.util.Date;

public class Employee {

    int id;
    String name;
    Date dateHired;
    EmployeeAttributes employeeAttributes;

    public String getName() {
        return name;
    }

    public Date getDateHired() {
        return dateHired;
    }

    public int getID() {
        return id;
    }

    public EmployeeAttributes getEmployeeAttributes(){
        return employeeAttributes;
    }
}
