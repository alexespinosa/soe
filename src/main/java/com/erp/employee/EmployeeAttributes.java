package com.erp.employee;

public class EmployeeAttributes {

    boolean managerFlag;
    boolean fullTimeFlag;

    public boolean isManager() {
        return managerFlag;
    }
    public boolean isFullTime() {
        return fullTimeFlag;
    }
}
