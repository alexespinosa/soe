package com.erp.soe.service;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;

@Service
public class PrimeService {

    /**
     * Feed into the reader an integer > 2.  Output will give you all the primes up to that number in the format
     * "prime prime ...'newline'".  Feed "stop" when you want to terminate.
     *
     * @param reader For input
     * @param writer Used for output
     * @throws IOException Thrown for reader/writer issues.
     */
    public void writePrimes(BufferedReader reader, BufferedWriter writer) throws IOException {
        String line;
        while (!"stop".equalsIgnoreCase(line = reader.readLine())) {
            if(isValidInput(line)) {
                writer.write(generatePrimes(line));
            } else{
                writer.write("Please enter an integer greater than 2.");
            }
            writer.newLine();
            writer.flush();
        }
    }

    /**
     * Will return all the primes up to the number given in the form of "prime prime ..."
     *
     * @param strNum - Integer in string format
     * @throws NumberFormatException Thrown if strNum is not an integer.
     * @return Primes in string format
     */
    public String generatePrimes(String strNum) {
        StringBuilder result = new StringBuilder();
        int input = Integer.parseInt(strNum);
        boolean[] primes = new boolean[input];
        Arrays.fill(primes, Boolean.TRUE);
        for(int i = 2; i < input/2; i++){
            if(primes[i]){
                for(int j = 2; j * i < input; j++){
                    primes[j*i] = false;
                }
            }
        }
        for(int i = 2; i < input; i++){
            if(primes[i]){
                result.append(i).append(" ");
            }
        }
        return result.toString();
    }

    /**
     * Return true if strNum is an integer > 2, false otherwise.
     *
     * @param strNum - Integer in string format
     * @return if valid input
     */
    protected boolean isValidInput(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int i = Integer.parseInt(strNum);
            return i > 2;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
