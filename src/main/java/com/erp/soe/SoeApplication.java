package com.erp.soe;

import com.erp.soe.service.PrimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

@SpringBootApplication
public class SoeApplication implements CommandLineRunner {

	@Autowired
	private PrimeService primeService;

	public static void main(String[] args) {
		SpringApplication.run(SoeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); BufferedWriter writer
				= new BufferedWriter(new OutputStreamWriter(System.out));){
			writer.write("Please provide an integer greater than 2, this will write out all the primes up to the input exclusively.  Input 'stop' to terminate.");
			writer.newLine();
			writer.flush();
			primeService.writePrimes(reader, writer);
		}
	}
}
